function draw() {
  afiseazaBackground();
  linii();
  afiseazaGUI();
  afisareTrageCarte();
  verificaCarteInManaAfisare();
  verificaCarteInMana();
  verificaCarti();
  
}

function afiseazaBackground() {
  u.deseneazaImagine(assetBackground, 0, 0, width, height);
}

function linii() {
  // Linie jucator 1.
  strokeWeight(2);
  stroke(0);
  line(0, height - 300, width, height - 300);

  // Linie jucator 2.
  strokeWeight(2);
  stroke(0);
  line(0, 300, width, 300);
}

function afiseazaGUI() {
  gui.afiseazaRand();
  gui.afiseazaNumarCartiRamasePachet();
}

function afisareTrageCarte() {
  let x = width / 2 + 100;
  let y = height / 2 - 75;

  u.deseneazaImagine(assetCarteIntoarsa, x, y, 100, 150);
}

function verificaCarteInMana() {
  // Verific daca am carti in mana.
  if (cartiInMana.length > 0) {
    // Setez BOOLamCartiInMana = true daca am carti in mana.
    BOOLamCartiInMana = true;
  }
  // Verific daca am carti in mana.
  else if (cartiInMana.length == 0) {
    // Setez BOOLamCartiInMana = false daca nu am carti in mana.
    BOOLamCartiInMana = false;
  }
}

function verificaCarteInManaAfisare() {
  // Verific proprietatea (inMana) a fiecarei carti din pachetul de carti al jucatorului 1 pentru a o afisa sau nu.
  for (let i = 0; i < cartiPlayerOne.length; i++) {
    let carteCurenta = cartiPlayerOne[i];
    carteCurenta.afiseazaLaPozitieCursor();
    carteCurenta.afiseazaLaPozitieFixa();
  }
  // Verific proprietatea (inMana) a fiecarei carti din pachetul de carti al jucatorului 2 pentru a o afisa sau nu.
  for (let i = 0; i < cartiPlayerTwo.length; i++) {
    let carteCurenta = cartiPlayerTwo[i];
    carteCurenta.afiseazaLaPozitieCursor();
    carteCurenta.afiseazaLaPozitieFixa();
  }
  // Verific proprietatea (inMana) a fiecarei carti din mana pentru a o afisa sau nu.
  if (BOOLamCartiInMana) {
    for (let i = 0; i < cartiInMana.length; i++) {
      let carteCurenta = cartiInMana[i];
      carteCurenta.afiseazaLaPozitieCursor();
      carteCurenta.afiseazaLaPozitieFixa();
    }
  }

  // Afisez ultima carte pusa jos.
  let carteCurenta = cartiPuseJos[cartiPuseJos.length - 1];
  carteCurenta.afiseazaLaPozitieFixa();
}

function verificaCarti() {
  let cartePusaJos = cartiPuseJos[cartiPuseJos.length - 1];
  if (BOOLamCartiInMana) {
    let carteInMana = cartiInMana[cartiInMana.length - 1];
    return (cartePusaJos.valoare == carteInMana.valoare || cartePusaJos.tip == carteInMana.tip && u.verificaPozitii(mouseX, mouseY, (w / 2) - 200, (w / 2) - 200 + 100, (h / 2) - 75, (h / 2) - 75 + 150));
  }
}