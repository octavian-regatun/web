class ListaCarti {

  constructor() {
    this.pachetCarti = [];
    this.tip;
    this.valoare;
    this.numarCarte = 0;
  }

  creareListaCarti() {
    for (let i = 0; i < 54; i++) {
      append(this.pachetCarti, new Carte());
    }
  }

  calculeazaTip() {
    let restValoare = this.numarCarte % 4;
    switch (restValoare) {
      case 0:
        this.tip = "Romb"
        break;
      case 1:
        this.tip = "Trefla"
        break;
      case 2:
        this.tip = "Inima"
        break;
      case 3:
        this.tip = "Frunza"
        break;
      default:
        this.tip = "Empty";
        break;
    }
    switch (this.numarCarte) {
      case 53:
        this.tip = "Rosu";
        break;
      case 54:
        this.tip = "Negru";
        break;
    }
  }

  calculeazaValoare() {
    let catValoare = floor(this.numarCarte / 4);
    this.numarCarte++;
    switch (catValoare) {
      case 0:
        this.valoare = "As";
        break;
      case 1:
        this.valoare = "2";
        break;
      case 2:
        this.valoare = "3";
        break;
      case 3:
        this.valoare = "4";
        break;
      case 4:
        this.valoare = "5";
        break;
      case 5:
        this.valoare = "6";
        break;
      case 6:
        this.valoare = "7";
        break;
      case 7:
        this.valoare = "8";
        break;
      case 8:
        this.valoare = "9";
        break;
      case 9:
        this.valoare = "10";
        break;
      case 10:
        this.valoare = "J";
        break;
      case 11:
        this.valoare = "Q";
        break;
      case 12:
        this.valoare = "K";
        break;
      case 13:
        this.valoare = "Joker";
        break;
      default:
        this.valoare = "Empty";
    }
  }

  adaugaCarti() {
    for (let i = 0; i < 54; i++) {
      this.calculeazaValoare();
      this.calculeazaTip();
      this.pachetCarti[i] = new Carte(this.valoare, this.tip);
    }
  }

  afisare() {
    console.log(this.pachetCarti);
  }

  amesteca() {
    let j = 0;
    while (j < 100) {
      shuffle(this.pachetCarti, true);
      j++;
    }
  }
}