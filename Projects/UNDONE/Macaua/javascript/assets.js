//#region assets

// let assetBackground;
// let assetCarteIntoarsa;
// let assetCarteRombAs;
// let assetCarteTreflaAs;
// let assetCarteInimaAs;
// let assetCarteFrunzaAs;
// let assetCarteTreflaDoi;
// let assetCarteInimaDoi;
// let assetCarteFrunzaDoi;
// let assetCarteRombDoi;
// let assetCarteTreflaTrei;
// let assetCarteInimaTrei;
// let assetCarteFrunzaTrei;
// let assetCarteRombTrei;
// let assetCarteTreflaPatru;
// let assetCarteInimaPatru;
// let assetCarteFrunzaPatru;
// let assetCarteRombPatru;
// let assetCarteTreflaCinci;
// let assetCarteInimaCinci;
// let assetCarteFrunzaCinci;
// let assetCarteRombCinci;
// let assetCarteTreflaSase;
// let assetCarteInimaSase;
// let assetCarteFrunzaSase;
// let assetCarteRombSase;
// let assetCarteTreflaSapte;
// let assetCarteInimaSapte;
// let assetCarteFrunzaSapte;
// let assetCarteRombSapte;
// let assetCarteTreflaOpt;
// let assetCarteInimaOpt;
// let assetCarteFrunzaOpt;
// let assetCarteRombOpt;
// let assetCarteTreflaNoua;
// let assetCarteInimaNoua;
// let assetCarteFrunzaNoua;
// let assetCarteRombNoua;
// let assetCarteTreflaZece;
// let assetCarteInimaZece;
// let assetCarteFrunzaZece;
// let assetCarteRombZece;
// let assetCarteTreflaJ;
// let assetCarteInimaJ;
// let assetCarteFrunzaJ;
// let assetCarteRombJ;
// let assetCarteTreflaQ;
// let assetCarteInimaQ;
// let assetCarteFrunzaQ;
// let assetCarteRombQ;
// let assetCarteTreflaK;
// let assetCarteInimaK;
// let assetCarteFrunzaK;
// let assetCarteRombK;
// let assetCarteRosuJoker;
// let assetCarteNegruJoker;

//#endregion assets

function incarcaAssets() {
  // BACKGROUND
  assetBackground = loadImage("https://i.imgur.com/UGbTgcb.jpg");
  // CARTE INTOARSA
  assetCarteIntoarsa = loadImage("assets/CarteIntoarsa.png");
  // AS
  assetCarteTreflaAs = loadImage("assets/AsTrefla.png");
  assetCarteInimaAs = loadImage("assets/AsInima.png");
  assetCarteFrunzaAs = loadImage("assets/AsFrunza.png");
  assetCarteRombAs = loadImage("assets/AsRomb.png");
  // DOI
  assetCarteTreflaDoi = loadImage("assets/DoiTrefla.png");
  assetCarteInimaDoi = loadImage("assets/DoiInima.png");
  assetCarteFrunzaDoi = loadImage("assets/DoiFrunza.png");
  assetCarteRombDoi = loadImage("assets/DoiRomb.png");
  // TREI
  assetCarteTreflaTrei = loadImage("assets/TreiTrefla.png");
  assetCarteInimaTrei = loadImage("assets/TreiInima.png");
  assetCarteFrunzaTrei = loadImage("assets/TreiFrunza.png");
  assetCarteRombTrei = loadImage("assets/TreiRomb.png");
  // PATRU
  assetCarteTreflaPatru = loadImage("assets/PatruTrefla.png");
  assetCarteInimaPatru = loadImage("assets/PatruInima.png");
  assetCarteFrunzaPatru = loadImage("assets/PatruFrunza.png");
  assetCarteRombPatru = loadImage("assets/PatruRomb.png");
  // CINCI
  assetCarteTreflaCinci = loadImage("assets/CinciTrefla.png");
  assetCarteInimaCinci = loadImage("assets/CinciInima.png");
  assetCarteFrunzaCinci = loadImage("assets/CinciFrunza.png");
  assetCarteRombCinci = loadImage("assets/CinciRomb.png");
  // SASE
  assetCarteTreflaSase = loadImage("assets/SaseTrefla.png");
  assetCarteInimaSase = loadImage("assets/SaseInima.png");
  assetCarteFrunzaSase = loadImage("assets/SaseFrunza.png");
  assetCarteRombSase = loadImage("assets/SaseRomb.png");
  // SAPTE
  assetCarteTreflaSapte = loadImage("assets/SapteTrefla.png");
  assetCarteInimaSapte = loadImage("assets/SapteInima.png");
  assetCarteFrunzaSapte = loadImage("assets/SapteFrunza.png");
  assetCarteRombSapte = loadImage("assets/SapteRomb.png");
  // OPT
  assetCarteTreflaOpt = loadImage("assets/OptTrefla.png");
  assetCarteInimaOpt = loadImage("assets/OptInima.png");
  assetCarteFrunzaOpt = loadImage("assets/OptFrunza.png");
  assetCarteRombOpt = loadImage("assets/OptRomb.png");
  // NOUA
  assetCarteTreflaNoua = loadImage("assets/NouaTrefla.png");
  assetCarteInimaNoua = loadImage("assets/NouaInima.png");
  assetCarteFrunzaNoua = loadImage("assets/NouaFrunza.png");
  assetCarteRombNoua = loadImage("assets/NouaRomb.png");
  // ZECE
  assetCarteTreflaZece = loadImage("assets/ZeceTrefla.png");
  assetCarteInimaZece = loadImage("assets/ZeceInima.png");
  assetCarteFrunzaZece = loadImage("assets/ZeceFrunza.png");
  assetCarteRombZece = loadImage("assets/ZeceRomb.png");
  // J
  assetCarteTreflaJ = loadImage("assets/JTrefla.png");
  assetCarteInimaJ = loadImage("assets/JInima.png");
  assetCarteFrunzaJ = loadImage("assets/JFrunza.png");
  assetCarteRombJ = loadImage("assets/JRomb.png");
  // Q
  assetCarteTreflaQ = loadImage("assets/QTrefla.png");
  assetCarteInimaQ = loadImage("assets/QInima.png");
  assetCarteFrunzaQ = loadImage("assets/QFrunza.png");
  assetCarteRombQ = loadImage("assets/QRomb.png");
  // K
  assetCarteTreflaK = loadImage("assets/KTrefla.png");
  assetCarteInimaK = loadImage("assets/KInima.png");
  assetCarteFrunzaK = loadImage("assets/KFrunza.png");
  assetCarteRombK = loadImage("assets/KRomb.png");
  // JOKER
  assetCarteRosuJoker = loadImage("assets/JokerRosu.png");
  assetCarteNegruJoker = loadImage("assets/JokerNegru.png");
}