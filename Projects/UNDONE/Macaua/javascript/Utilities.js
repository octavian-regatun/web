class Utilities {
    deseneazaImagine(_image, _x, _y, _size1, _size2) {
        _image.resize(_size1, _size2);
        image(_image, _x, _y);
    }
    
    verificaPozitii(_x, _y, _X1, _X2, _Y1, _Y2) {
        // coliziune, events
        //         1 - limita coliziune mica (prima)
        //         2 - limita coliziune mare (a doua)
        // fara caps - limita coliziune interior
        //      CAPS - limita coliziune exterior
        return (_X1 < _x && _x < _X2 && _Y1 < _y && _y < _Y2);
      }
}