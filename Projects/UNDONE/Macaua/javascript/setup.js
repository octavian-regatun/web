function preload() {
  incarcaAssets();
}

function setup() {
  createCanvas(w, h);

  listaCarti.adaugaCarti();
  listaCarti.amesteca();

  distribuieCarti();
  primaCarte();
  
  // pentru debugging
  listaCarti.pachetCarti[43] = new Carte("Joker", "Rosu");
}

function mousePressed() {
  trageCarteClickEvent();
  puneCarteJosClickEvent();
  iaCarteInManaClickEvent();
  gameLogicClickEvent();
}

function distribuieCarti() {
  // Pozitie x pentru fiecare carte.
  let x1 = width / 2 - 350;
  let x2 = width / 2 - 200;
  let x3 = width / 2 - 50;
  let x4 = width / 2 + 100;
  let x5 = width / 2 + 250;
  // Lista de pozitii x.
  let xPozitii = [];
  // Pozitii x jucator 1.
  xPozitii[53] = x1;
  xPozitii[52] = x2;
  xPozitii[51] = x3;
  xPozitii[50] = x4;
  xPozitii[49] = x5;
  // Pozitii x jucator 2.
  xPozitii[48] = x1;
  xPozitii[47] = x2;
  xPozitii[46] = x3;
  xPozitii[45] = x4;
  xPozitii[44] = x5;
  // Pozitie y jucator 1.
  let yPoz1 = height / 2 + 175;
  // Pozitie y jucator 2.
  let yPoz2 = 75;

  for (let i = listaCarti.pachetCarti.length - 1; i >= listaCarti.pachetCarti.length - 5; i--) {
    let carteCurenta = listaCarti.pachetCarti[i];
    carteCurenta.x = xPozitii[i];
    carteCurenta.y = yPoz1;
    carteCurenta.inMana = false;
    carteCurenta.player = 1;
    append(cartiPlayerOne, carteCurenta);
  }

  let n = 0;

  while (n < 5) {
    shorten(listaCarti.pachetCarti);
    n++;
  }

  for (let i = listaCarti.pachetCarti.length - 1; i >= listaCarti.pachetCarti.length - 5; i--) {
    let carteCurenta = listaCarti.pachetCarti[i];
    carteCurenta.x = xPozitii[i];
    carteCurenta.y = yPoz2;
    carteCurenta.inMana = false;
    carteCurenta.player = 2;
    append(cartiPlayerTwo, carteCurenta);
  }

  n = 0;

  while (n < 5) {
    shorten(listaCarti.pachetCarti);
    n++;
  }
}

function primaCarte() {
  let n = 1;
  let bool = false;

  // let cartePusaJos;

  //#region  -----------------WIP------------------
  // while (bool == false) {
  //   if (listaCarti.pachetCarti[listaCarti.pachetCarti.length - n].valoare == "Joker") {
  //     console.log("test");
  //     bool = false;
  //   } else if (listaCarti.pachetCarti[listaCarti.pachetCarti.length - n].valoare != "Joker") {
  //     bool = true;
  //   }
  // }
//#endregion

  // // Prima carte pusa jos este ultima carte din pachet.
  let cartePusaJos = listaCarti.pachetCarti[listaCarti.pachetCarti.length - 1];
  // // Declar pozitia unde vreau sa fie afisata prima carte pe masa.


  cartePusaJos.x = w / 2 - 200;
  cartePusaJos.y = h / 2 - 75;
  // // Adaug prima carte pusa jos in pachetCarti cu carti puse jos.
  cartiPuseJos.push(cartePusaJos);
  // // Prima carte pusa jos o scot din pachetul de inceput.
  shorten(listaCarti.pachetCarti);
  // // Setez proprietatea primei carti puse jos ca nefiind in mana (false).
  cartePusaJos.inMana = false;
}

function trageCarteClickEvent() {
  // Verifica daca dau click pe locul (cartea) de luat carti.
  if (BOOLamCartiInMana == false) {
    if (u.verificaPozitii(mouseX, mouseY, (w / 2) + 100, (w / 2) + 100 + 100, (h / 2) - 75, (h / 2) - 75 + 150) == true) {
      // Declar ca variabila carteCurenta sa fie ultima carte din pachet.
      let carteCurenta = listaCarti.pachetCarti[listaCarti.pachetCarti.length - 1];
      // Setez valoare cartii luate din pachet ca fiind in mana.
      // Afisez cartea la pozitia cursorului (am cartea in mana) din cauza ca am setat proprietatea inMana = true.
      carteCurenta.inMana = true;
      // Setez proprietatea cartii a cui jucator apartine.
      carteCurenta.player = rand;
      // Adaug carteCurenta in pachetCarti cu carti in mana(cartiInMana).
      append(cartiInMana, listaCarti.pachetCarti[listaCarti.pachetCarti.length - 1]);
      // Scot carteaCurenta din pachet.
      shorten(listaCarti.pachetCarti);
    }
  }
}

function puneCarteJosClickEvent() {
  // Verifica daca dau click pe partea jucatorului 1.
  if (u.verificaPozitii(mouseX, mouseY, 0, width, height - 300, height)) {
    // Verifica daca am carti in mana.
    if (BOOLamCartiInMana == true) {
      // Citesc pachetCarti cu cartiInMana.
      for (let i = cartiInMana.length - 1; i >= 0; i--) {
        // Declar ca variabila carteCurenta sa fie ultima carte in mana.
        let carteCurenta = cartiInMana[i];
        // Transfer cartea din mana in pachetul jucatorului 1.
        append(cartiPlayerOne, carteCurenta);
        // Scot cartea din mana.
        shorten(cartiInMana);
        // Setez valoare cartii luate din pachet ca nefiind in mana (false).
        carteCurenta.inMana = false;
      }
    }
  }
  // Verifica daca dau click pe partea jucatorului 2.
  if (u.verificaPozitii(mouseX, mouseY, 0, width, 0, 300)) {
    // Verifica daca am carti in mana.
    if (BOOLamCartiInMana == true) {
      // Citesc pachetCarti cu cartiInMana.
      for (let i = cartiInMana.length - 1; i >= 0; i--) {
        // Declar ca variabila carteCurenta sa fie ultima carte in mana.
        let carteCurenta = cartiInMana[i];
        // Transfer cartea din mana in pachetul jucatorului 2.
        append(cartiPlayerTwo, carteCurenta);
        // Scot cartea din mana.
        shorten(cartiInMana);
        // Setez valoare cartii luate din pachet ca nefiind in mana (false).
        carteCurenta.inMana = false;
      }
    }
  }
}

function iaCarteInManaClickEvent() {
  // Verifica daca am carti in mana.
  if (BOOLamCartiInMana == false) {
    // Citesc pachetCarti cu carti jucator 1.
    for (let i = 0; i < cartiPlayerOne.length; i++) {
      // Declar ca variabila carteCurenta sa fie cartea de pozitie "i" din pachetul de carti al jucatorului 1 (pachetCarti).
      let carteCurenta = cartiPlayerOne[i];
      // Verific daca dau click pe cartea respectiva (carteCurenta).
      if (u.verificaPozitii(mouseX, mouseY, carteCurenta.x, carteCurenta.x + 100, carteCurenta.y, carteCurenta.y + 150)) {
        // Transfer cartea din pachetul jucatorului 1 in pachetCarti cu cartiInMana.
        append(cartiInMana, carteCurenta);
        // Scot cartea respectiva din pachetul jucatorului 1.
        cartiPlayerOne.splice(i, 1);
        // Afisez cartea la pozitia cursorului (am cartea in mana) din cauza ca am setat proprietatea inMana = true.
        carteCurenta.inMana = true;
      }
    }

    for (let i = 0; i < cartiPlayerTwo.length; i++) {
      // Declar ca variabila carteCurenta sa fie cartea de pozitie "i" din pachetul de carti al jucatorului 1 (pachetCarti).
      let carteCurenta = cartiPlayerTwo[i];
      // Verific daca dau click pe cartea respectiva (carteCurenta).
      if (u.verificaPozitii(mouseX, mouseY, carteCurenta.x, carteCurenta.x + 100, carteCurenta.y, carteCurenta.y + 150)) {
        // Transfer cartea din pachetul jucatorului 1 in pachetCarti cu cartiInMana.
        append(cartiInMana, carteCurenta);
        // Scot cartea respectiva din pachetul jucatorului 1.
        cartiPlayerTwo.splice(i, 1);
        // Afisez cartea la pozitia cursorului (am cartea in mana) din cauza ca am setat proprietatea inMana = true.
        carteCurenta.inMana = true;
      }
    }
  }
}

function gameLogicClickEvent() {
  if (u.verificaPozitii(mouseX, mouseY, (w / 2) - 200, (w / 2) - 200 + 100, (h / 2) - 75, (h / 2) - 75 + 150) == true && verificaCarti() == true) {
    console.log("test");
  }
}