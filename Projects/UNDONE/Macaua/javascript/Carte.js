class Carte {
  constructor(_valoare, _tip) {
    this.tip = _tip;
    this.valoare = _valoare;
    this.inMana = undefined;
    this.player = undefined;
    this.x = undefined;
    this.y = undefined;
  }

  //#region algoritm aflaAssetCorespunzator

  aflaAssetCorespunzator() {
    if (this.tip == "Trefla" && this.valoare == "As")
      this.asset = assetCarteTreflaAs;
    else if (this.tip == "Trefla" && this.valoare == "2")
      this.asset = assetCarteTreflaDoi;
    else if (this.tip == "Trefla" && this.valoare == "3")
      this.asset = assetCarteTreflaTrei;
    else if (this.tip == "Trefla" && this.valoare == "4")
      this.asset = assetCarteTreflaPatru;
    else if (this.tip == "Trefla" && this.valoare == "5")
      this.asset = assetCarteTreflaCinci;
    else if (this.tip == "Trefla" && this.valoare == "6")
      this.asset = assetCarteTreflaSase;
    else if (this.tip == "Trefla" && this.valoare == "7")
      this.asset = assetCarteTreflaSapte;
    else if (this.tip == "Trefla" && this.valoare == "8")
      this.asset = assetCarteTreflaOpt;
    else if (this.tip == "Trefla" && this.valoare == "9")
      this.asset = assetCarteTreflaNoua;
    else if (this.tip == "Trefla" && this.valoare == "10")
      this.asset = assetCarteTreflaZece;
    else if (this.tip == "Trefla" && this.valoare == "J")
      this.asset = assetCarteTreflaJ;
    else if (this.tip == "Trefla" && this.valoare == "K")
      this.asset = assetCarteTreflaK;
    else if (this.tip == "Trefla" && this.valoare == "Q")
      this.asset = assetCarteTreflaQ;
    else if (this.tip == "Frunza" && this.valoare == "As")
      this.asset = assetCarteFrunzaAs;
    else if (this.tip == "Frunza" && this.valoare == "2")
      this.asset = assetCarteFrunzaDoi;
    else if (this.tip == "Frunza" && this.valoare == "3")
      this.asset = assetCarteFrunzaTrei;
    else if (this.tip == "Frunza" && this.valoare == "4")
      this.asset = assetCarteFrunzaPatru;
    else if (this.tip == "Frunza" && this.valoare == "5")
      this.asset = assetCarteFrunzaCinci;
    else if (this.tip == "Frunza" && this.valoare == "6")
      this.asset = assetCarteFrunzaSase;
    else if (this.tip == "Frunza" && this.valoare == "7")
      this.asset = assetCarteFrunzaSapte;
    else if (this.tip == "Frunza" && this.valoare == "8")
      this.asset = assetCarteFrunzaOpt;
    else if (this.tip == "Frunza" && this.valoare == "9")
      this.asset = assetCarteFrunzaNoua;
    else if (this.tip == "Frunza" && this.valoare == "10")
      this.asset = assetCarteFrunzaZece;
    else if (this.tip == "Frunza" && this.valoare == "J")
      this.asset = assetCarteFrunzaJ;
    else if (this.tip == "Frunza" && this.valoare == "Q")
      this.asset = assetCarteFrunzaQ;
    else if (this.tip == "Frunza" && this.valoare == "K")
      this.asset = assetCarteFrunzaK;
    else if (this.tip == "Inima" && this.valoare == "As")
      this.asset = assetCarteInimaAs;
    else if (this.tip == "Inima" && this.valoare == "2")
      this.asset = assetCarteInimaDoi;
    else if (this.tip == "Inima" && this.valoare == "3")
      this.asset = assetCarteInimaTrei;
    else if (this.tip == "Inima" && this.valoare == "4")
      this.asset = assetCarteInimaPatru;
    else if (this.tip == "Inima" && this.valoare == "5")
      this.asset = assetCarteInimaCinci;
    else if (this.tip == "Inima" && this.valoare == "6")
      this.asset = assetCarteInimaSase;
    else if (this.tip == "Inima" && this.valoare == "7")
      this.asset = assetCarteInimaSapte;
    else if (this.tip == "Inima" && this.valoare == "8")
      this.asset = assetCarteInimaOpt;
    else if (this.tip == "Inima" && this.valoare == "9")
      this.asset = assetCarteInimaNoua;
    else if (this.tip == "Inima" && this.valoare == "10")
      this.asset = assetCarteInimaZece;
    else if (this.tip == "Inima" && this.valoare == "J")
      this.asset = assetCarteInimaJ;
    else if (this.tip == "Inima" && this.valoare == "Q")
      this.asset = assetCarteInimaQ;
    else if (this.tip == "Inima" && this.valoare == "K")
      this.asset = assetCarteInimaK;
    else if (this.tip == "Romb" && this.valoare == "As")
      this.asset = assetCarteRombAs;
    else if (this.tip == "Romb" && this.valoare == "2")
      this.asset = assetCarteRombDoi;
    else if (this.tip == "Romb" && this.valoare == "3")
      this.asset = assetCarteRombTrei;
    else if (this.tip == "Romb" && this.valoare == "4")
      this.asset = assetCarteRombPatru;
    else if (this.tip == "Romb" && this.valoare == "5")
      this.asset = assetCarteRombCinci;
    else if (this.tip == "Romb" && this.valoare == "6")
      this.asset = assetCarteRombSase;
    else if (this.tip == "Romb" && this.valoare == "7")
      this.asset = assetCarteRombSapte;
    else if (this.tip == "Romb" && this.valoare == "8")
      this.asset = assetCarteRombOpt;
    else if (this.tip == "Romb" && this.valoare == "9")
      this.asset = assetCarteRombNoua;
    else if (this.tip == "Romb" && this.valoare == "10")
      this.asset = assetCarteRombZece;
    else if (this.tip == "Romb" && this.valoare == "J")
      this.asset = assetCarteRombJ;
    else if (this.tip == "Romb" && this.valoare == "K")
      this.asset = assetCarteRombK;
    else if (this.tip == "Romb" && this.valoare == "Q")
      this.asset = assetCarteRombQ;
    else if (this.tip == "Rosu" && this.valoare == "Joker")
      this.asset = assetCarteRosuJoker;
    else if (this.tip == "Negru" && this.valoare == "Joker")
      this.asset = assetCarteNegruJoker;


    return this.asset;

  }

  //#endregion algoritm aflaAssetCorespunzator

  afiseazaLaPozitieCursor() {
    if (this.inMana) {
      this.x = Math.floor(mouseX) - 50;
      this.y = Math.floor(mouseY) - 75;
      u.deseneazaImagine(this.aflaAssetCorespunzator(), this.x, this.y, 100, 150);
    }
  }

  afiseazaLaPozitieFixa() {
    if (this.inMana == false) {
      this.x = this.x;
      this.y = this.y;
      u.deseneazaImagine(this.aflaAssetCorespunzator(), this.x, this.y, 100, 150);
    }
  }
}