let minge;
let paletaDreapta;
let paletaStanga;
let coliziunePaleta;
let culoare;
let angle;
let xPozitivNegativ;
let yPozitivNegativ;
let bariera;
let slider;
let valoareSlider;

function setup() {
  createCanvas(600, 600);
  minge = new Minge();
  paletaDreapta = new PaletaDreapta();
  paletaStanga = new PaletaStanga();
  bariera = new Bariera();

  slider = createSlider(1, 60, 60, 1);

  minge.directieRandom();
}

function draw() {
  background(51);
  minge.afiseaza();
  minge.misca();
  minge.verificaMargine();
  // minge.verificaSusJos();
  paletaStanga.afiseaza();
  paletaStanga.keyPressed();
  paletaStanga.coliziune();
  paletaDreapta.misca();
  paletaDreapta.afiseaza();
  paletaDreapta.coliziune();
  bariera.afiseaza();
  bariera.misca();
  bariera.margine();
  bariera.coliziune();
  debug();
  afiseazaText();
  scor();
  resetGame();
  sliderFrameRate();
}

//debug

function debug() {
  fill("magenta");
  ellipse(paletaDreapta.x, paletaDreapta.y, 5, 5);
  ellipse(paletaDreapta.x, paletaDreapta.y + 43, 5, 5);
  ellipse(paletaDreapta.x, paletaDreapta.y + 86, 5, 5);
  ellipse(paletaDreapta.x, paletaDreapta.y + 129, 5, 5);

  ellipse(paletaStanga.x + 12, paletaStanga.y, 5, 5);
  ellipse(paletaStanga.x + 12, paletaStanga.y + 43, 5, 5);
  ellipse(paletaStanga.x + 12, paletaStanga.y + 86, 5, 5);
  ellipse(paletaStanga.x + 12, paletaStanga.y + 129, 5, 5);

  ellipse(width - 33, minge.y, 5, 5);

  fill(0, 0, 255);
  ellipse(paletaDreapta.x + 6, paletaDreapta.y + 21, 5, 5);
  ellipse(paletaDreapta.x + 6, paletaDreapta.y + 65, 5, 5);
  ellipse(paletaDreapta.x + 6, paletaDreapta.y + 108, 5, 5);

  // ellipse(paletaStanga.x + 12, paletaStanga.y, 5, 5);
  // ellipse(paletaStanga.x + 12, paletaStanga.y + 43, 5, 5);
  // ellipse(paletaStanga.x + 12, paletaStanga.y + 86, 5, 5);
  // ellipse(paletaStanga.x + 12, paletaStanga.y + 129, 5, 5);
}

//debug

function scor() {
  if (minge.x < 0) {
    minge.scorDreapta++;
    minge.pozitieDefault();
    minge.vitezaDefault();
    bariera.pozitieDefault();
  }
  if (minge.x > width) {
    minge.scorStanga++;
    minge.pozitieDefault();
    minge.vitezaDefault();
    bariera.pozitieDefault();
  }
}

function afiseazaText() {
  textSize(100);
  fill(255, 255, 255, 100);
  text(minge.scorStanga, 100, 100);
  text(minge.scorDreapta, 450, 100);
}

function resetGame() {
  if (minge.scorStanga > 11 || minge.scorDreapta > 11) {
    minge.scorStanga = 0;
    minge.scorDreapta = 0;
    minge.pozitieDefault();
    minge.vitezaDefault();
    bariera.pozitieDefault();
  }
}

function sliderFrameRate() {
  valoareSlider = slider.value();
  frameRate(valoareSlider);

  fill("white");
  textSize(32);
  text("FPS: " + valoareSlider, width / 2 - 50, 100)
}