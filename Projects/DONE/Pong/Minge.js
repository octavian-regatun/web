class Minge {

  constructor() {
    this.x = width / 2;
    this.y = height / 2;
    this.xRandom = 1.5;
    this.yRandom = 2;
    this.scorStanga = 0;
    this.scorDreapta = 0;
    this.xDirectie = floor(random(0, 2));
    this.yDirectie = floor(random(0, 2));
    this.vineDeSus = false;
    this.vineDeJos = false;
  }
  pozitieDefault() {
    this.x = width / 2;
    this.y = height / 2;
  }

  afiseaza() {
    fill("red");
    ellipse(this.x, this.y, 12, 12);
  }
  misca() {
    this.x += this.xRandom;
    this.y += this.yRandom;
  }
  verificaMargine() {
    if (this.y < 6 || this.y > height - 6) {
      this.yRandom *= -1;
    }
  }

  directieRandom() {
    if (this.xDirectie === 0) {
      this.xRandom *= -1;
    }
    if (this.yDirectie === 0) {
      this.yRandom *= -1;
    }
  }
  cresteViteza() {
    this.xRandom++;
    this.yRandom++;
  }
  vitezaDefault() {
    this.xRandom = 1.5;
    this.yRandom = 2;
  }
  // verificaSusJos() {
  //   if (minge.x === width - 33) {
  //     if (minge.y > paletaDreapta.y + 86) {
  //       this.vineDeJos = true;;
  //     }
  //     if (minge.y < paletaDreapta.y + 43) {
  //       this.vineDeSus = true;
  //     }
  //   }
  // }
}