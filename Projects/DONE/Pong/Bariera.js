class Bariera {
  constructor() {
    this.x = width / 2;
    this.y = height / 2 + 100;
    this.yRandom = 1;

  }
  afiseaza() {
    fill(0);
    rect(this.x, this.y, 10, 100);
  }
  misca() {
    this.y += this.yRandom;
  }
  coliziune() {
    if (minge.x + 6 > this.x && minge.x - 6 < this.x + 10 && minge.y > this.y && minge.y < this.y + 100) {
      minge.xRandom *= -1;
    }
  }
  margine() {
    if (this.y < 0 || this.y > height - 100) {
      this.yRandom *= -1;
    }
  }
  pozitieDefault() {
    this.x = width / 2;
    this.y = height / 2 + 100;
  }
}